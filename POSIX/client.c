//
// Created by pkalawski on 18.04.16.
//



#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <mqueue.h>
#include <string.h>
#include <signal.h>
#include <stdbool.h>
#include <unistd.h>

#define BUF_SIZE 512
char * private_name;

void handler(int a){
    mq_unlink(private_name);
    signal(SIGINT,SIG_DFL);
    raise(SIGINT);
}

bool is_prime(int number)
{
    if(number < 2) {
        return false;
    }
    if(number==2 || number==3) {
        return true;
    }
    for(unsigned i=2; i*i<=number; ++i) {
        if(number%i==0) {
            return false;
        }
    }
    return true;
}
int main(int argc, char **argv){

    //sprawdzam czy liczba argumentow wywolania sie zgadza
    if(argc != 3){
        fprintf(stderr,"Uzycie: %s [nazwa_serwera] [prywatna_nazwa]");
        exit(-1);
    }

    private_name = argv[2];
    int o_flag = O_WRONLY;
    int private_o_flag = O_CREAT|O_RDONLY;

    mqd_t msqid ;
    mqd_t private_msqid;

    struct mq_attr attr;
    attr.mq_msgsize = 512;
    attr.mq_maxmsg = 10;//podobno do 10 nie trzeba mieć ustawien roota
    attr.mq_curmsgs = 0;
    attr.mq_flags = 0;

    if((msqid = mq_open(argv[1],o_flag)) == -1){
        fprintf(stderr,"Nie mozna otworzyc kolejki serwera!\n");
        exit(-1);
    }

    if((private_msqid = mq_open(private_name,private_o_flag,0666,&attr)) == -1){
        fprintf(stderr,"Nie mozna otworzyc prywatnej kolejki!\n");
        exit(-1);
    }

    //ustawiam sobie bufor na dane

    char buffor[BUF_SIZE];
    buffor[0] = '1';
    strncpy(buffor+1,private_name,BUF_SIZE-1);

    //wysylamy wiadomosc inicjujaca

    if(mq_send(msqid,buffor,strlen(buffor)+1,1) == -1){
        fprintf(stderr,"Nie mozna wyslac wiadomosci inicjujacej!\n");
        exit(-1);
    }

    //Oczekujemy na odpowiedz od serwera

    if(mq_receive(private_msqid,buffor,sizeof(buffor),NULL) == -1){
        fprintf(stderr,"Nie mozna odebrac wiadomosci zwrotnej od serwera!\n");
        exit(-1);
    }

    //przechowujemy prywartne id otrzymane od serwera

    size_t private_id = (size_t) atoi(buffor);

    fprintf(stderr, "private id: %zu\n",private_id);
    char prv_id[BUF_SIZE];
    sprintf(prv_id, "%zu",private_id);

    signal(SIGINT,&handler);

    //oczekujemy na dalsze informacje od serwera w petli

    while(1){
        //numer komunikatu ustawiamy na 2
        buffor[0] = '2';
        strcpy(buffor+1,prv_id);

        if(mq_send(msqid,buffor,strlen(buffor) +1, 1) == -1){
            fprintf(stderr,"Nie udalo sie wyslac wiadomosci do serwera2!\n");
        }
        //oczekujemy na liczbe od serwera
        //printf("Oczekuje na odpowiedz od serwera\n");
        if(mq_receive(private_msqid,buffor,sizeof(buffor),NULL) == -1){
            fprintf(stderr,"Nie udalo sie odebrac liczby od serwera!\n");
        }

        //printf("Otrzymalem odpowiedz od serwera!\n");
        int number = atoi(buffor);

        if(number){

            bool result = is_prime(number);

            //ustawiamy typ komunikatu na 3
            buffor[0] = '3';
            sprintf(buffor+1, "%zu %d %d",private_id, number, result);

            if(mq_send(msqid,buffor,strlen(buffor)+1,1) == -1){
                fprintf(stderr,"Nie udalo sie odeslac odpowiedzi serwerowi!\n");
            }
        }
        sleep(1);
    }
}