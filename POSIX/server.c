#include <sys/signal.h>
#include <mqueue.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX_CLIENTS 512
#define BUF_SIZE 1024

//Typy komunikatow
//1 - nowy klient
//2 - czeka na liczbe
//3 - zwraca rezultat


const char* server_name;

void handler(int signum ) {

    mq_unlink(server_name);

    signal(SIGINT, SIG_DFL);
    raise(SIGINT);

}

int main(int argc, char* argv[]) {

    if(argc != 2){
        fprintf(stderr,"Uzycie: %s [nazwa_servera]\n",argv[0]);
        exit(-1);
    }
    server_name = argv[1];
    struct mq_attr attr;
    attr.mq_maxmsg = 10;
    attr.mq_msgsize = BUF_SIZE;
    attr.mq_flags = 0;
    attr.mq_curmsgs = 0;

    //id serwera
    mqd_t msqid;

    //flagi do otwarcia kolejki
    int mq_flag = O_RDONLY |O_CREAT;
    if((msqid = mq_open(server_name,mq_flag,0666,&attr)) == -1){
        fprintf(stderr,"Nie mozna utworzyc kolejki dla serwera!\n ");
        exit(-1);
    }

    //tablica deskrytporow dla klientow
    int clients[MAX_CLIENTS];
    int client_index = 1;

    signal(SIGINT,handler);

    //oczekuje na wiadomosci od klientow

    while(1){

        char buffor[BUF_SIZE];
        if(mq_receive(msqid,buffor,BUF_SIZE,NULL) == -1){
            fprintf(stderr,"Nie mozna odebrac wiadomosci od klienta!\n");
        }

        //kazdy z typow komunikatu

        if(buffor[0] == '1'){
            mqd_t mqd = mq_open(buffor+1,O_WRONLY);
            clients[client_index] = mqd;
            fprintf(stderr,"Odebralismy wiadomosc od nowego klienta: %s\n",buffor+1);

            //odsylamy prywatne id dla klienta
            sprintf(buffor,"%d",client_index);

            //klient do ktorego odsylamy wiadomosc
            int to_send = clients[client_index];

            if(mq_send(to_send,buffor,strlen(buffor)+1,1) == -1){
                fprintf(stderr,"Nie mozna odeslac wiadomosci do klienta!\n");
            }

            client_index++;
        }
        else if(buffor[0] == '2' ){
            int client_id = atoi(buffor+1);
            fprintf(stderr,"Po liczbe zglosil sie klient numer: %d\n",client_id);

            unsigned num = (unsigned int) ((rand() % 1000000) + 1);
            sprintf(buffor,"%u", num);

            fprintf(stderr,"WYslano liczbe %s klientowi numer: %d\n",buffor,clients[client_id]);

            mq_send(clients[client_id],buffor,strlen(buffor)+1,1);

        }
        else if(buffor[0] == '3'){
            int client_id, number, prime;

            sscanf(buffor+1,"%d %d %d",&client_id,&number, &prime);

            fprintf(stderr,"Liczba: %d klient(%d)\n",number,client_id);
        }
    }

}