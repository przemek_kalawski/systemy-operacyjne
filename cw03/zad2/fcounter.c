#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <dirent.h>
#include <unistd.h>
#include <sys/wait.h>

extern char **environ;

void throw_error(const char *message);

char *remove_trailing_slash(char *a);

int checkext(char * name, char * extension) {
    char * name_ext = strrchr(name, '.');
    if (!name_ext) {
        return 1;
    }
    if (strstr(name_ext, extension) != NULL) {
        return 0;
    }
    return 1;
}


int main(int argc, char **argv) {
    char * path, *extension;
    int vflag = 0, wflag = 0;
    int count = 0, child_count = 0;
    int child_proces = 0;
    int status;

    //sprawdzam czy zmienna srodowiskowa jest ustalona
    if(!(path =  getenv("PATH_TO_BROWSE")) || strcmp(path, " ") == 0){
        path = ".";
    }

    extension = getenv("EXTENSION_TO_BROWSE");

    DIR * directory;
    if(!(directory = opendir(path))){
        throw_error("Cannot open directory!\n");
    }

    //Sprawdzam czy jest jakas opcja wpisana w linii polecen
        if (argc != 1) {
            for (int i = 0; i < argc; ++i) {
                if (strcmp(argv[i], "-v") == 0) {
                    vflag = 1;
                }
                if (strcmp(argv[i], "-w") == 0) {
                    wflag = 1;
                }
            }
        }


    struct dirent *str_dirent;
    char * new_path;
    char *file_name;
    pid_t PID;

    while ((str_dirent = readdir(directory)) != 0 ){
        file_name = str_dirent->d_name;
        if (strcmp(file_name, ".") == 0 || strcmp(file_name, "..") == 0) {
            continue;
        }
        if (!extension || checkext(file_name, extension) == 0) {
            ++count;
        }

        //sprawdzam czy plik jest katalogiem
        if(str_dirent->d_type ==  DT_DIR){
            new_path = malloc(PATH_MAX * sizeof(char));
            sprintf(new_path,"%s/%s",remove_trailing_slash(path), file_name);
            setenv("PATH_TO_BROWSE",new_path,1);
            PID = fork();

            if(PID<0){
                exit(-1);
            }
            else if( PID == 0){
                execve("fcounter",argv,environ);
            }
            if(PID != 0){
                child_proces +=1;
            }
        }
    }
    closedir(directory);
    for (int i = 0; i < child_proces; ++i) {

        wait(&status);
        child_count +=WEXITSTATUS(status);
        child_proces -=1;
    }

    if(vflag == 1){
        printf("Sciezka %s, liczba plikow: %d, liczba plikow w potomkach: %d.\n",path,count,count + child_count);
    }

    if (wflag == 1){
        sleep(7);
    }


    exit(count + child_count);
    return EXIT_SUCCESS;
}


void throw_error(const char *message)
{
    perror(message);
    exit(1);
}

char *remove_trailing_slash(char *a)
{
    size_t len = strlen(a);
    if (len > 0 && a[len - 1] == '/') {
        a[len - 1] = '\0';
    }

    return a;
}