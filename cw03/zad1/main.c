#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <sys/wait.h>
#include <sched.h>
#include <sys/time.h>
#include <sys/resource.h>
#include <unistd.h>
#include <bits/time.h>
#include <time.h>
#include <string.h>

//#define FORK
//#define VFORK
//#define CLONE
//#define CLONEV

typedef struct{
    double real;
    double user;
    double sys;
} times_t;

times_t get_times(enum __rusage_who type);

long int global_variable = 0;

int increment_counter(void * child_stack);

void print_time(times_t start, times_t end);

int main(int argc, char **argv) {

    int status;
    pid_t PID;

    if(argc < 2){
        printf("Check arguments in command line.\n");
        return -1;
    }
    long n;
    n = strtol(argv[1],NULL,10);
    if (n < 1){
        printf("Argument must be higher than 1!");
        return -1;
    }


    times_t start_parent = get_times(RUSAGE_SELF);
    times_t start_child = get_times(RUSAGE_CHILDREN);

#if defined(CLONE)|| defined(CLONEV)
    void * child_stack = malloc(1024);
                child_stack +=1024;
#endif

    for (int i = 0; i < n; ++i) {
        #ifdef FORK
                PID = fork();
        #elif defined(VFORK)
                PID = vfork();
        #endif
        #if defined(FORK)|| defined(VFORK)

        if(PID < 0) {
            printf("Fork false!\n");
            exit(1);
        }else if(PID == 0){
            global_variable++;
            exit(0);
        }
        else{
            waitpid(PID, &status, 0);
        }

#endif

#if defined(CLONE) || defined(CLONEV)
#ifdef CLONE
        PID = clone(increment_counter,child_stack,SIGCHLD| CLONE_FS ,NULL);
#elif defined(CLONEV)
        PID = clone(increment_counter,child_stack,SIGCHLD|CLONE_FILES|CLONE_FS|CLONE_VM,NULL);
#endif

        waitpid(PID,&status,0);
#endif
    }
    printf("Licznik wynosi: %ld\n", global_variable);
    times_t end_parent = get_times(RUSAGE_SELF);
    times_t end_child = get_times(RUSAGE_CHILDREN);

    puts("Parent times");
    print_time(start_parent, end_parent);
    puts("Children times:");
    print_time(start_child, end_child);

    return 0;
}


int increment_counter(void *child_stack){
    global_variable++;
    return 0;
}

void print_time(times_t start, times_t end)
{
    printf(
            "| real\t%fs\n" "| user\t%fs\n" "| sys\t%fs\n\n",
            end.real - start.real, end.user - start.user, end.sys - start.sys
    );

}

times_t get_times(enum __rusage_who type)
{
    times_t times;

    struct rusage rusage;
    clock_t my_clock;

    my_clock = clock();
    getrusage(type, &rusage);

    times.real = my_clock / (double) CLOCKS_PER_SEC;
    times.user = rusage.ru_utime.tv_sec + rusage.ru_utime.tv_usec / (double) 10e5;
    times.sys = rusage.ru_stime.tv_sec + rusage.ru_stime.tv_usec / (double) 10e5;

    return times;
}