//
// Created by pkalawski on 22.05.16.
//

#ifndef SOCKET1_MAIN_H_H
#define SOCKET1_MAIN_H_H

#define MAX_USERNAME_LEN 20
#define MAX_MSG_LEN 200
#define MESSAGE_STRUCT_SIZE MAX_USERNAME_LEN + MAX_MSG_LEN + 2

struct Message {
    char username[MAX_USERNAME_LEN + 1];
    char message[MAX_MSG_LEN + 1];
};
#endif //SOCKET1_MAIN_H_H
