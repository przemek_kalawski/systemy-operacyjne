#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <ctype.h>

//This program work like "tr '[:lower:]' '[:upper:]' | fold -w N"

#define BUFFER 2048

void read_from_pipe (int file, const int N) {

    char* buffer = malloc(sizeof(char)*BUFFER);

    FILE *stream;
    char c;
    int counter = 0;
    stream = fdopen (file, "r");
    while ((c = (char) fgetc (stream)) != EOF)

        if(counter == N) {
            buffer[counter] = '\0';
            fputs(buffer, stdout);
            fputc('\n',stdout);
            counter = 0;
            buffer[counter] = c;
            ++counter;
        }
        else if(c == '\n') {

            buffer[counter] = c;
            buffer[++counter] = '\0';

            fputs(buffer, stdout);
            counter = 0;

        }
        else {
            buffer[counter] = c;
            ++counter;
        }
    if(counter) {
        buffer[++counter] = '\0';
        fputs(buffer,stdout);
    }

    fclose (stream);
}

void write_to_pipe (int file) {

    FILE *stream;
    stream = fdopen (file, "w");

    //Read from stdin
    char c;
    while((c = (char) getchar()) != EOF) {
        if(islower(c)) {
            c = (char) toupper(c);
        }
        fprintf(stream, "%c",c);
    }

    fclose (stream);
}

int main(int argc, char* argv[]) {

    if(argc<2) {
        fprintf(stderr, "Usage %s [N]\n", argv[0]);
        exit(EXIT_FAILURE);
    }

    int N = atoi(argv[1]);
    if(N < 1) {
        fprintf(stderr, "Invalid N argument: %s\n", argv[1]);
    }
    if(N > BUFFER) {
        fprintf(stderr, "N argument is too large: %s\n", argv[1]);
    }


    int PID;
    int fd[2];

    /* Create the pipe. */
    if (pipe (fd))
    {
        fprintf (stderr, "Pipe failed.\n");
        return EXIT_FAILURE;
    }

    /* Create the child process. */
    PID = fork ();
    if (PID == (pid_t) 0)
    {
        //Child process
        //Close descriptor
        close (fd[1]);
        read_from_pipe (fd[0], N);
        return EXIT_SUCCESS;
    }
    else if (PID <  0)
    {
        fprintf (stderr, "Fork failed.\n");
        return EXIT_FAILURE;
    }
    else
    {
        //Parent process
        //close descriptor
        close (fd[0]);
        write_to_pipe (fd[1]);
        return EXIT_SUCCESS;
    }

}