#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include <malloc.h>
#include <string.h>
#include <time.h>
#include <stdlib.h>


#define MAX_BUF 1024


int main()
{
    int fd;
    char * myfifo = "/tmp/myfifo";

    /* create the FIFO (named pipe) */
    mkfifo(myfifo, 0666);
    char * string;

    time_t t;
    struct tm tm;

    char *PID = calloc(20,sizeof(char));
    char * times;
    char * min;
    char * sec;


    char * message;

    while(1) {
        time(&t);
        times = calloc(30,sizeof(char));
        min = calloc(10,sizeof(char) );
        sec = calloc(10,sizeof(char));
        message = calloc(MAX_BUF,sizeof(char));
        string = calloc(MAX_BUF,sizeof(char));
        read(STDIN_FILENO, string, MAX_BUF);
        string[strlen(string) - 1] = (char) '\0';
        strcpy(message,"PID procesu: ");
        sprintf(PID, "%ld", (long)getpid());
        strcat(message,PID);
        strcat(message,"\n");
        tm = *localtime(&t);
        sprintf(times,"%ld",(long)tm.tm_hour);
        sprintf(min,":%ld",(long)tm.tm_min);
        sprintf(sec,":%ld\n",(long)tm.tm_sec);
        strcat(message,times);
        strcat(message,min);
        strcat(message,sec);
        strcat(message,string);

        if (strcmp(string, "koniec") == 0) {
            fd = open(myfifo, O_WRONLY);
            write(fd, message, strlen(message));
            free(string);
            close(fd);
            /* remove the FIFO */
            unlink(myfifo);

            return 0;
        }
        fd = open(myfifo, O_WRONLY);
        write(fd, message, strlen(message));
        close(fd);
        free(string);
        free(message);
        //free(time);
        string = NULL;
    }
}