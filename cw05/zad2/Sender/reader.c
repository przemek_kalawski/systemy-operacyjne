#include <fcntl.h>
#include <stdio.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#define MAX_BUF 1024

int main()
{
    int fd;
    char * myfifo = "/tmp/myfifo";
    char *buf;
    char * message;
    time_t t;
    struct tm tm;
    char * times;
    char * min;
    char * sec;

    /* open, read, and display the message from the FIFO */
    while(1) {
        time(&t);
        message = calloc(MAX_BUF,sizeof(char));
        times = calloc(30,sizeof(char));
        min = calloc(10,sizeof(char) );
        sec = calloc(10,sizeof(char) );
        fd = open(myfifo, O_RDONLY);
        buf = calloc(MAX_BUF,sizeof(char) );
        read(fd, buf, MAX_BUF);
        tm = *localtime(&t);
        sprintf(times,"%ld",(long)tm.tm_hour);
        sprintf(min,":%ld",(long)tm.tm_min);
        sprintf(sec,":%ld\t",(long)tm.tm_sec);
        strcpy(message,"Received time: ");
        strcat(message,times);
        strcat(message,min);
        strcat(message,sec);
        strcat(message,buf);
        if(strlen(buf) > 0) {
            printf("Received: %s\n", message);
            free(buf);
            free(message);
        }
        if(strcmp(buf,"koniec") == 0){
            free(buf);
            close(fd);
            printf("Koniec pracy programu\n");
            return 0;
        }
        close(fd);
    }

}