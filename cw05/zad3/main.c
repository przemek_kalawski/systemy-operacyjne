#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(int argc, char **argv) {
    FILE *in;
    extern FILE *popen();
    char buff[512];
    char *wywolanie = "ls -l | grep ^d > ";
    char *pelne_wywolanie = malloc(sizeof(char) * (strlen(wywolanie) + strlen(argv[1])));
    strcpy(pelne_wywolanie,wywolanie);
    strcat(pelne_wywolanie,argv[1]);

    if(!(in = popen(pelne_wywolanie,"r"))){
        exit(1);
    }

    while(fgets(buff, sizeof(buff), in)!=NULL){
        printf("%s", buff);
    }
    pclose(in);

}