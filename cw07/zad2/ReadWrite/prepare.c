//
// Created by pkalawski on 23.04.16.
//
#include <sys/types.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <semaphore.h>
#include <fcntl.h>
#include <signal.h>
#include <stdbool.h>
#include <unistd.h>
#include <sys/mman.h>
#include <time.h>


#define BLOCK_SIZE 50
#define MAX_READERS 8

sem_t* sem_rd;
sem_t* sem_wr;
int shmfd;
int* shared_segment;
const char* sem_name;
const char* sem_name2;
const char* shm_name;

void sig_handler(int signum)
{
    munmap(shared_segment, BLOCK_SIZE);
    shm_unlink(shm_name);
    sem_unlink(sem_name);
    sem_unlink(sem_name2);
    signal(signum, SIG_DFL);
    raise(signum);
}

int main(int argc, char* argv[]) {

    //sprawdzmy liczbe argumentow
    if (argc != 4) {
        fprintf(stderr, "Uzycie: %s [semafor czytelnika] [semafor pisarza] [nazwa pamieci]\n", argv[0]);
        exit(-1);
    }

    
    sem_name = argv[1];
    sem_name2 = argv[2];
    shm_name = argv[3];

    int flgs = 0666;
    umask(0);
    //tworzymy semafor czytelnika
    if((sem_rd = sem_open(argv[1], O_CREAT | O_EXCL, flgs, MAX_READERS)) == SEM_FAILED)
    {
        fprintf(stderr,"Nie można utworzyc semafora czytelnika!\n");
        exit(-1);
    };

    fprintf(stderr, "Semafor czytelnika %s zostal utworzony\n", sem_name);
    //tworzymy semafor 
    if((sem_wr = sem_open(argv[2], O_CREAT | O_EXCL, flgs, 1)) == SEM_FAILED)
    {
        fprintf(stderr,"Nie mozan utworzyc semafora pisarza!\n");
        exit(-1);
    };

    fprintf(stderr, "Semafor pisarza %s zostal utworzony\n", sem_name2);

    if ((shmfd = shm_open(argv[3], O_CREAT | O_EXCL | O_RDWR, (mode_t)flgs)) == -1) {
        fprintf(stderr,"Nie udalo sie utworzyc wspolnego segmentu pamieci!\n");
        exit(-1);
    }

    if((ftruncate(shmfd, sizeof(int)*BLOCK_SIZE))==-1) {
        fprintf(stderr,"Nie udalo sie ustalic rozmiaru segmentu!\n");
        exit(-1);
    }

    if((shared_segment = mmap(NULL, sizeof(int)*BLOCK_SIZE, PROT_WRITE | PROT_READ, MAP_SHARED, shmfd, 0)) == MAP_FAILED) {
        fprintf(stderr,"Nie udalo sie zmapowac obszaru pamieci!\n");
        exit(-1);
    };

    //rozmiar segmentu pamięci
    shared_segment[0] = BLOCK_SIZE - 1;

    for(int i=1; i<BLOCK_SIZE; ++i)
        shared_segment[i] = -1;


    signal(SIGINT, sig_handler);

    while(true) {
        sleep(10);
    }
}