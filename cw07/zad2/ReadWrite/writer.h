//
// Created by pkalawski on 25.04.16.
//

#include <sys/ipc.h>
#include <sys/sem.h>
#include <sys/types.h>
#include <semaphore.h>


#define SHARED_MEMORY_NAME "/so08zad2"
#define LIBRARY 100
#define SEMAPHORE_NAME "/mutex"
#define SEMAPHORE_NAME2 "/no_writers"
#define N 100
#define ERROR { int error_code = errno; \
				perror("blad: "); \
				exit(error_code); }
				
struct problem
{
    int data[N];
};

#define MEM_SIZE sizeof(struct problem)


