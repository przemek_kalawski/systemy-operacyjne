
#include <sys/types.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <semaphore.h>
#include <fcntl.h>
#include <signal.h>
#include <stdbool.h>
#include <unistd.h>
#include <sys/mman.h>
#include <time.h>
#include <sys/time.h>


#define BLOCK_SIZE 50
#define RAND_LIMIT 32
#define MAX_READERS 8

sem_t* sem_rd;
sem_t* sem_wr;
int shmfd;
int* shared_data;
const char* shm_name;

void sig_handler(int signum)
{

    sem_close(sem_rd);
    sem_close(sem_wr);
    signal(signum, SIG_DFL);
    raise(signum);
}

int main(int argc, char* argv[]) {

    srand(time(NULL));

    if (argc != 4) {
        fprintf(stderr, "Uzycie: %s [semafor czytelnika] [semafor pisarza] [nazwa pamieci]\n", argv[0]);
        exit(-1);
    }

    shm_name = argv[2];

    int flgs = 0666;
    if((sem_rd = sem_open(argv[1], 0, flgs, 1))==SEM_FAILED)
    {
        fprintf(stderr,"Nie mozan pozyskac semafora czytelnika!\n");
        exit(-1);
    };

    if((sem_wr = sem_open(argv[2], 0, flgs, 1))==SEM_FAILED)
    {
        fprintf(stderr,"Nie mozan pozyskac semafora pisarza!\n");
        exit(-1);
    };

    if ((shmfd = shm_open(argv[3], O_RDWR, (mode_t)flgs)) == -1) {
        fprintf(stderr,"Nie udalo sie utworzyc wspolnego segmentu pamieci!\n");
        exit(-1);
    }

    if((shared_data = mmap(NULL, BLOCK_SIZE, PROT_READ | PROT_WRITE, MAP_SHARED, shmfd, 0)) == MAP_FAILED) {
        fprintf(stderr,"Nie udalo sie zmapowac obszaru pamieci!\n");
        exit(-1);
    };
    
    
    const int* const arr_size = shared_data;

    ++shared_data;

    signal(SIGINT, sig_handler);

    struct timeval tv;
    char time_buf[80];

    while(true) {

        int to_insert = rand()%(*arr_size);
        int* indexes = malloc(sizeof(int)*to_insert);

        for(int i=0; i<to_insert; ++i) {
            indexes[i] = rand()%(*arr_size);
        }

        //Stop writers
        sem_wait(sem_wr);

        //Stop readers
        for(int i=0; i<MAX_READERS; ++i){
            sem_wait(sem_rd);
        }

        //insert or increase numbers
        for(int i=0; i<to_insert; ++i) {
            (shared_data[indexes[i]] == -1) ? shared_data[indexes[i]] = rand()%RAND_LIMIT
                                            : (shared_data[indexes[i]] = (shared_data[indexes[i]]+1)%RAND_LIMIT);
            gettimeofday(&tv, NULL);
            strftime(time_buf, sizeof(time_buf), "%H:%M:%S:", localtime(&tv.tv_sec));
            printf("(%d %s%lums) Wpisałem liczbę %d na pozycję %d. Pozostało %d zadań.\n",
                   getpid(), time_buf, tv.tv_usec/1000, shared_data[indexes[i]], indexes[i], to_insert-i);
        }

        //unlock readers
        for(int i=0; i<MAX_READERS; ++i){
            sem_post(sem_rd);
        }
        //unlock writers
        sem_post(sem_wr);

        free(indexes);

        sleep((unsigned int) (rand() % 3));
    }
}
