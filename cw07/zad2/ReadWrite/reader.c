//
// Created by pkalawski on 23.04.16.
//


#include <sys/types.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <semaphore.h>
#include <fcntl.h>
#include <signal.h>
#include <stdbool.h>
#include <unistd.h>
#include <sys/mman.h>
#include <time.h>
#include <sys/time.h>


#define BLOCK_SIZE 50

sem_t*sem_rd;
int shmfd;
int* shared_data;
const char* shm_name;

void sig_handler(int signum)
{
    sem_close(sem_rd);
    signal(signum, SIG_DFL);
    raise(signum);
}

int main(int argc, char* argv[]) {

    srand(time(0));

    if (argc != 4) {
        fprintf(stderr, "Uzycie: %s [semafor czytelnika] [nazwa pamieci] [liczba]\n", argv[0]);
        exit(-1);
    }

    int val = atoi(argv[3]);
    if(val <= 0) {
        fprintf(stderr, "Zła wartosc dla poszukiwanej liczby! %s\n", argv[3]);
        exit(-1);
    }
    
    shm_name = argv[2];

    int flgs = 0666;
    if((sem_rd = sem_open(argv[1], 0, flgs, 1)) == SEM_FAILED)
    {
        fprintf(stderr,"Nie udalo sie odczytac semafora!\n");
        exit(-1);
    };

    if ((shmfd = shm_open(argv[2], O_RDONLY, (mode_t)flgs)) == -1) {
        fprintf(stderr,"Nie udalo sie odczytac segmentu pamięci!\n");
        exit(-1);
    }

    if((shared_data = mmap(NULL, BLOCK_SIZE, PROT_READ, MAP_SHARED, shmfd, 0)) == MAP_FAILED) {
        fprintf(stderr,"Nie udalo sie odwzorowac pamieci wspolnej na obszar procesu!\n");
        exit(-1);
    };



    const int* const arr_size = shared_data;
    ++shared_data;


    signal(SIGINT, sig_handler);

    struct timeval tv;
    char time_buf[80];

    while(true) {

        sem_wait(sem_rd);

        int cnt = 0;
        for(int i=0; i<*arr_size; ++i) {
            if(shared_data[i]==val) {
                ++cnt;
            }
        }

        gettimeofday(&tv, NULL);
        strftime(time_buf, sizeof(time_buf), "%H:%M:%S:", localtime(&tv.tv_sec));
        printf("(%d %s%lums) Znalazłem %d liczb o wartości %d.\n",
               getpid(), time_buf, tv.tv_usec/1000, cnt, val);

        sem_post(sem_rd);

        sleep((unsigned int) (rand() % 3));

    }
}