#include <sys/types.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/sem.h>
#include <signal.h>
#include <stdbool.h>
#include <unistd.h>
#include <sys/time.h>
#include <time.h>

#define BLOCK_SIZE 10

int* shared_segment;

void sig_handler(int signum __attribute__ ((unused)))
{
    shmdt(shared_segment - 3);

    signal(signum, SIG_DFL);
    raise(signum);
}

int main(int argc, char* argv[]) {

    srand((unsigned int) time(NULL));

    //sprawdzamy czy zostały podane potrzebne agumenty do zadania
    if(argc != 3 ) {
        fprintf(stderr, "Użycie: %s [sciezka] [proj_id]\n", argv[0]);
        exit(-1);
    }

    int proj_id = atoi(argv[2]);

    if(!proj_id) {
        fprintf(stderr, "Zły project id %s\n", argv[2]);
        exit(-1);
    }

    
    //Pobieramy klucz do segmentu danych współdzielonych do zaalokowania
    key_t key = ftok(argv[1], proj_id);

    //ustawiamy flagi dostępu
    int flgs = 0666;
    //pobieramy id bloku który alokujemy
    
    int shmid;
    if((shmid = shmget(key, sizeof(int) * BLOCK_SIZE, flgs)) == -1) {
        fprintf(stderr,"Błąd zaalokowania segmentu danych!\n");
        exit(-1);
    }

    int semid;
    if((semid = semget(key, 1, 0)) == -1) {
        fprintf(stderr, "Nie udalo sie utworzyc semafora!\n");
        exit(-1);
    }

    

    if(*(shared_segment = shmat(shmid, 0, 0))==-1) {
        fprintf(stderr, "Pobieramy id segmentu który będziemy używać do współdzielenia danych!\n");
        exit(-1);
    }

    const int* const arr_size = shared_segment;
    ++shared_segment;

    int * const insert_index = shared_segment;
    ++shared_segment;

    int* const remove_index = shared_segment;
    ++shared_segment;


    signal(SIGINT, sig_handler);

    //ogranicza dostęp do zasobów
    struct sembuf sem_lock = {0,-1,0 };
    //zwalnia dostęp do zasobów
    struct sembuf sem_unlock = {0,1,0 };

    struct timeval tv;
    char time_buf[80];

    printf("Rozpoczynamy produkcje!\n");

    while(true) {

        int insert_number = rand();

        if((semop(semid, &sem_lock, 1)) == -1) {
            fprintf(stderr,"Nie mozna opuścić semafora dostępu!\n");
        }

        if((*insert_index) >= (*arr_size)) {
            (*insert_index) = 0;
        }

        //Sprawdzamy ile jest już wyprodukowanych danych
        if((*insert_index +1)%(*arr_size) == (*remove_index)) {
            fprintf(stderr,"Nie mozna więcej produkować na tę chwilę\n");
            goto end;
        }

        shared_segment[(*insert_index)] = insert_number;
        ++(*insert_index);

        int oper_num = (*insert_index) - (*remove_index);
        if(oper_num < 0) {
            oper_num += *arr_size;
        }
        gettimeofday(&tv, NULL);
        strftime(time_buf, sizeof(time_buf), "%H:%M:%S:", localtime(&tv.tv_sec));
        printf("(%d %s%lums) Dodałem liczbę %d na pozycję %d. Liczba zadań oczekujących: %d.\n",
               getpid(), time_buf, tv.tv_usec/1000, insert_number, (*insert_index)-1, oper_num);

        end:
        if((semop(semid, &sem_unlock, 1))==-1) {
            fprintf(stderr,"Nie można podnieść semafora i udostępnić dostępu do danych!\n");

        }
        sleep((unsigned int) (rand() % 3));
    }


}