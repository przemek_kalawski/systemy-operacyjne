#include <sys/types.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/sem.h>
#include <signal.h>
#include <stdbool.h>
#include <unistd.h>
#include <sys/time.h>
#include <time.h>


#define BLOCK_SIZE 10

int shmid;
int semid;

void sig_handler(int signum __attribute__ ((unused)))
{

    shmctl(shmid, IPC_RMID, 0);
    semctl(semid, IPC_RMID, 0);

    signal(signum, SIG_DFL);
    raise(signum);
}

int main(int argc, char* argv[]) {

    srand((unsigned int) time(NULL));

    if(argc != 3 ) {
        fprintf(stderr, "Uzycie: %s [sciezka] [proj_id]\n", argv[0]);
        exit(-1);
    }

    int proj_id = atoi(argv[2]);

    if(!proj_id) {
        fprintf(stderr, "Zły project id %s\n", argv[2]);
        exit(-1);
    }


    //Pobieramy klucz do segmentu danych współdzielonych do zaalokowania
    key_t key = ftok(argv[1], proj_id);

    //ustawiamy flagi dostępu
    int flgs = 0666;
    //pobieramy id bloku który alokujemy
    int shmid;
    if ((shmid = shmget(key, sizeof(int) * BLOCK_SIZE, flgs | IPC_CREAT | IPC_EXCL)) == -1) {
        fprintf(stderr, "Nie udalo sie utworzyc semafora!\n");
        exit(-1);
    }

    int* shared_segment;
    if(*(shared_segment = shmat(shmid, 0, 0))==-1) {
        fprintf(stderr, "Pobieramy id segmentu który będziemy używać do współdzielenia danych!\n");
        exit(-1);
    }

    // W piwrszym segmencie jest rozmiar bloku danych, w kolejnych indeksy
    shared_segment[0] = BLOCK_SIZE - 3;
    shared_segment[1] = 0;
    shared_segment[2] = -1;



    if((semid = semget(key, 1, flgs | IPC_CREAT | IPC_EXCL)) == -1) {
        fprintf(stderr,"Nie udalo sie utworzyc semafora!\n");
        exit(-1);
    }

    struct sembuf sem_unlock = {0,1,0 };
    if((semop(semid, &sem_unlock, 1))==-1) {
        fprintf(stderr,"Nie udało się podniesc semafora dostępu!\n");
    }


    signal(SIGINT, sig_handler);

    while(true) {
        sleep(1);
    }
}