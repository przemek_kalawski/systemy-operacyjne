//
// Created by pkalawski on 09.03.16.
//

#include "veryficationFile.h"


static char * rights_to_files(struct stat fileStat , char * file_name){

    char * file_right = malloc(sizeof(char)* 10);

    stat(file_name, &fileStat);

    if((fileStat.st_mode & S_IRUSR)){
        strcat(file_right,"r");
    }
    else{
        strcat(file_right,"-");
    }
    if((fileStat.st_mode & S_IWUSR)){
        strcat(file_right,"w");
    }
    else{
        strcat(file_right,"-");
    }
    if((fileStat.st_mode & S_IXUSR)){
        strcat(file_right,"x");
    }
    else{
        strcat(file_right,"-");
    }
    if((fileStat.st_mode & S_IRGRP)){
        strcat(file_right,"r");
    }
    else{
        strcat(file_right,"-");
    }
    if((fileStat.st_mode & S_IWGRP)){
        strcat(file_right,"w");
    }
    else{
        strcat(file_right,"-");
    }
    if((fileStat.st_mode & S_IXGRP)){
        strcat(file_right,"x");
    }
    else{
        strcat(file_right,"-");
    }
    if((fileStat.st_mode & S_IROTH)){
        strcat(file_right,"r");
    }
    else{
        strcat(file_right,"-");
    }
    if((fileStat.st_mode & S_IWOTH)){
        strcat(file_right,"w");
    }
    else{
        strcat(file_right,"-");
    }
    if((fileStat.st_mode & S_IXOTH)){
        strcat(file_right,"x");
    }
    else{
        strcat(file_right,"-");
    }
    return file_right;
}


void print_file_details(char * file_name, char *access_rights, char * file){

    struct stat fileStat ;
    char * file_rights;

    stat(file_name, &fileStat);


    if(S_ISDIR(fileStat.st_mode)){
        open_directory(file_name,access_rights,file);
    }
    else {

        file_rights = rights_to_files(fileStat,file_name);


        /*assert(strcmp(rights_to_files,access_rights));*/
/*        printf("Information for %s\n", file_name);*/

/*        printf("%ld", (long) (fileStat->st_mode&0777));*/

/*        if((fileStat->st_mode&0777) == access_rights) {*/




        if(strcmp(access_rights,file_rights) == 0) {
            printf("Information for %s\n", file_name);
            printf("---------------------------\n");
            printf("File Size: \t\t%d bytes\n", (int) fileStat.st_size);
            printf("Time of last access: \t\t %d \n", fileStat.st_atim);
            printf("Path to file: \t\t%s \n", file);
            printf("\n");
        }
        //}

        free(file_rights);

    }
}


void open_directory(char * path, char *access_rights, char * path_from_file){

    DIR *dp;
    struct dirent *ep;
    dp = opendir (path);
    char * copy_path;
    copy_path = malloc(sizeof(char) * 2 * strlen(path));
    strcpy(copy_path,path);

    char * file = malloc(sizeof(char) * 50);

    if (dp != NULL)
    {
        while ((ep = readdir(dp))) {

            strcpy(copy_path,path);
            strcat(copy_path,"/");
            strcpy(file,path_from_file);
            strcat(file,ep->d_name);
            strcat(file,"/");
            strcat(copy_path,ep->d_name);
/*            printf("%s\n",copy_path);*/
            if(strcmp(ep->d_name,".") && strcmp(ep->d_name,"..")) {
                /*printf("%s\n",file);*/
                print_file_details(copy_path, access_rights,file);
            }
        }

        (void) closedir (dp);
    }
    else
        perror ("Couldn't open the directory");

    free(copy_path);
}

