#define _XOPEN_SOURCE 500
#include <ftw.h>
#include "veryficationFile.h"

void print_directory_list(char *file_path);


unsigned long right_from_command;

int main(int argc, char **argv)
{



    right_from_command = strtoul(argv[2], NULL, 8);

    if(argc != 3)
        return 1;


    if(strlen(argv[2]) != 4){
        return 1;
    }
    int x;
    char y;
    char * buffer = malloc(sizeof(char) * 10);
    for (int i = 1; i < 4; ++i) {
        y = argv[2][i];
        x = atoi(&y);
        switch (x) {
            case 7:
                strcat(buffer, "rwx");
                break;
            case 6:
                strcat(buffer, "rw-");
                break;
            case 5:
                strcat(buffer, "r-x");
                break;
            case 4:
                strcat(buffer,"r--");
                break;
            case 2:
                strcat(buffer,"-w-");
                break;
            case 1:
                strcat(buffer,"--x");
                break;
            case 0:
                strcat(buffer,"---");
                break;
            default:break;
        }
    }


    open_directory(argv[1],buffer,"../");

    print_directory_list(argv[1]);


    return 0;
}


static int wrapper(const char * fpath, const struct stat *sb, int typeflag, struct FTW *ftwbuf) {
/*    if(typeflag == FTW_D){
        printf("%s\n",fpath);
    }*/

    struct stat tmp = *sb;

    if(typeflag == FTW_F && (sb->st_mode&0777) == right_from_command && sb->st_dev && sb->st_ino){

        printf("---------------------------\n");
        printf("File Size: \t\t%d bytes\n", (int) tmp.st_size);
        printf("Time of last access: \t\t %d \n", (int) tmp.st_atime);
        printf("Path to file: \t\t%s \n",fpath);
        printf("\n");

    }

    return(0);
}

void print_directory_list(char *file_path) {

    int flags = 0;
    nftw(file_path, wrapper, 10, flags);
}

