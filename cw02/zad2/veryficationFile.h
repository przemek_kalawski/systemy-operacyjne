//
// Created by pkalawski on 09.03.16.
//

#ifndef ODCZYTZPLIKU_VERYFICATIONFILE_H
#define ODCZYTZPLIKU_VERYFICATIONFILE_H

#include <unistd.h>
#include <stdio.h>
#include <sys/stat.h>
#include <dirent.h>
#include <string.h>
#include <stdlib.h>
#include <assert.h>


void print_file_details(char * file_name, char *access_rights, char * file);
void open_directory(char * path, char *access_rights, char * path_from_file);

#endif //ODCZYTZPLIKU_VERYFICATIONFILE_H
