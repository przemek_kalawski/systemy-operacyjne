//
// Created by pkalawski on 10.03.16.
//

#ifndef LIBSYSFUNCTION_LIBRARY_FUN_H
#define LIBSYSFUNCTION_LIBRARY_FUN_H

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/resource.h>
#include <sys/stat.h>
#include <stdbool.h>
#include <unistd.h>
#include <fcntl.h>

void open_file_to_read(char * file_name, size_t record_long, int opt_sort);

void get_times(double *user, double *sys);

#endif //LIBSYSFUNCTION_LIBRARY_FUN_H
