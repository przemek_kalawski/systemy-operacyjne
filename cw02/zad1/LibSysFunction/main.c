#include <string.h>
#include <stdio.h>
#include "library_fun.h"





int main(int argc, char **argv) {

    if( argc != 4){
        return -1;
    }
    size_t record_size = (size_t) atoi(argv[2]);

/*    printf("%s\n",argv[0]);*/



    double user_start, sys_start, user_end, sys_end;

    get_times(&user_start, &sys_start);

    if(strcmp(argv[3],"sys") == 0){
        printf("Systemowe\n");
        open_file_to_read(argv[1], record_size,0);
    }
    else if(strcmp(argv[3],"lib") == 0){
        printf("Biblioteczne\n");
        open_file_to_read(argv[1], record_size,1);
    }


    get_times(&user_end, &sys_end);

    printf("| user\t%fs\n" "| sys\t%fs\n", user_end - user_start, sys_end - sys_start);

    return 0;
}