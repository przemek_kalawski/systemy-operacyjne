//
// Created by pkalawski on 10.03.16.
//

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include "library_fun.h"

static size_t int2size_t(size_t val) {
    return (val < 0) ? __SIZE_MAX__ : (size_t)((unsigned)val);
}

void open_file_to_read(char * file_name, size_t record_long, int opt_sort){

    struct stat my_stat;
    int records_number;

    stat(file_name, &my_stat);
    records_number = (int) (my_stat.st_size / record_long);


    char * buffer = malloc(sizeof(char) * record_long);
    char * buffer_to_compare = malloc(sizeof(char) * record_long);

    if(opt_sort) {
        FILE * file = fopen(file_name,"r+");
        if(!file){
            printf("Taki plik nie istnieje.\n");
            return;
        }

        for (int i = 0; i < records_number; ++i) {
            fseek(file, 2 * sizeof(int), SEEK_SET);

            for (int j = 0; j < records_number - i; ++j) {
                fseek(file, j * sizeof(char) * record_long, SEEK_SET);
                fread(buffer, sizeof(char), record_long, file);
                fread(buffer_to_compare, sizeof(char), record_long, file);

                if (*buffer > *buffer_to_compare) {

                    fseek(file, j * sizeof(char) * record_long, SEEK_SET);
                    fwrite(buffer_to_compare, sizeof(char), record_long, file);
                    fwrite(buffer, sizeof(char), record_long, file);


                }
            }
        }

        fseek(file,0,0);

        fclose(file);
    }
    else{
        int file = open(file_name, O_RDWR);
        for (int i = 0; i < records_number; ++i) {
            lseek(file, 2*sizeof(int), SEEK_SET);

            for (int j = 0; j < records_number - i; ++j) {
                lseek(file, j * sizeof(char)*record_long, SEEK_SET);
                read(file, buffer, record_long);
                read(file, buffer_to_compare, record_long);

                if (*buffer > *buffer_to_compare) {

                    lseek(file, j * sizeof(char)*record_long, SEEK_SET);
                    write(file, buffer_to_compare, record_long);
                    write(file, buffer, record_long);


                }
            }
        }

        lseek(file,0,0);

      /*  while(read(file,buffer,30)) {
            printf("%s\n", buffer);
        }*/
        close(file);
    }



    free(buffer);
    free(buffer_to_compare);


}



//times

void get_times(double *user, double *sys)
{
    struct rusage rusage;
    getrusage(RUSAGE_SELF, &rusage);

    *user = rusage.ru_utime.tv_sec + rusage.ru_utime.tv_usec / (double) 10e5;
    *sys = rusage.ru_stime.tv_sec + rusage.ru_stime.tv_usec / (double) 10e5;
}


