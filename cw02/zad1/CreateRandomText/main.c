#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int main(int argc, char ** argv) {

    if(argc != 4){
        return -1;
    }
    int number_of_records = 0;
    int record_long = 0;

    record_long = (int) strtoul(argv[2], NULL, 10);

    number_of_records = (int) strtoul(argv[3], NULL, 10);



    printf("%d\n",number_of_records);

    srand((unsigned int) time (NULL));

    FILE * file;
    file = fopen(argv[1], "wb+");
    if( !file ) {
        return  -1;
    }


    for(int j = 0; j < number_of_records; j++) {
        char * string = malloc(sizeof(char) * record_long);
        for (int i = 0; i < record_long; i++) {
            string[i] = (char) (rand() % 25 + 65);
        }
        fwrite(string, sizeof(char), (size_t) record_long, file);

        free(string);
        /*printf("%s\n", string);*/
    }



    fclose(file);

    return 0;
}