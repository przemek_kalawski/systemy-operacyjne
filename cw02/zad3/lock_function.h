//
// Created by pkalawski on 12.03.16.
//

#ifndef SETTHEFILELOCK_LOCK_FUNCTION_H
#define SETTHEFILELOCK_LOCK_FUNCTION_H


#include <fcntl.h>

pid_t lock_test(int fd, int type, off_t offset, int whence, int len);

int lock_reg(int fd, int cmd, int type, off_t offset, int whence, off_t len);

void test_bytes(int fd);

short lock_type(int fd, int byte);


//lock or unlock bytes

int lock_read(int file,int  offset,int  whence, int len);

int lock_write(int file,int  offset,int  whence, int len);

int unlock(int file,int  offset,int  whence, int len);


//is lockable byte

pid_t lockable_write(int file,int offset,int whence,int len);

pid_t lockable_read(int file,int offset,int whence,int len);

#endif //SETTHEFILELOCK_LOCK_FUNCTION_H
