#include <stdio.h>
#include <fcntl.h>
#include <errno.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/stat.h>
#include "print_fun.h"


int main(int argc, char ** argv) {

    if(argc < 2){
        printf("Cant't open file!\n");
        return -1;
    }

    int file = open(argv[1],O_RDWR);
    int error_number = errno;

    if(file < 0){
        printf("Failed. Cannot open file!\n");
        exit(error_number);
    }
    int file_size;
    struct stat my_stat;
    stat(argv[1], &my_stat);
    file_size = (int) my_stat.st_size;

    printf("\n>Write '7' to show option in program.\n ");
    check_bytes(file, file_size);
    close(file);

    return 0;
}

