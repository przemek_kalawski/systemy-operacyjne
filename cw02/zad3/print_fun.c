//
// Created by pkalawski on 12.03.16.
//

#include <stdio.h>
#include "print_fun.h"
#include "lock_function.h"

void print_option()
{
    puts("1: Lock byte reading");
    puts("2: Lock byte writing");
    puts("3: Remove lock");
    puts("4: List locked bytes");
    puts("5: Read byte");
    puts("6: Write byte");
    puts("7: Write '7' to show option in program.");
    puts("0: Exit");
}


int get_byte_index(int file_size)
{
    int byte;

    printf("Gimme byte index: ");
    scanf("%d", &byte);

    if (byte < 0 || file_size < byte) {
        puts("> Umm... byte index is higher than file size");
        return -1;
    }

    return byte;
}

int get_option()
{
    int option;

    printf("\nMy choice: ");
    scanf("%d", &option);

    return option;
}

void check_bytes(int file, int file_size) {

    int option;
    unsigned char buffer;
    while ((option = get_option())) {
        int offset;
        int pid;

        switch (option) {
            case 1: // Lock byte reading
                offset = get_byte_index(file_size);
                pid = lockable_read(file, offset, SEEK_SET, 1);
                if (pid  == 0) {
                    if (lock_read(file, offset, SEEK_SET, 1) < 0)
                        printf("Cannot lock byte!\n");
                    else
                        printf("%d byte was locked\n", offset);
                }
                else printf("We cannot lock this byte: PID=%d\n", pid);
                break;


            case 2: // Lock byte writing
                offset = get_byte_index(file_size);
                pid = lockable_write(file, offset, SEEK_SET, 1);
                if (pid == 0) {
                    if (lock_write(file, offset, SEEK_SET, 1) < 0) {
                        printf("Cannot lock byte.\n");
                    }
                    else {
                        printf("%d byte was locked\n ", offset);
                    }
                }
                else {
                    printf("We cannot lock this byte:: PID=%d\n", pid);
                }

            case 3: // Remove lock
                offset = get_byte_index(file_size);
                pid = unlock(file, offset, SEEK_SET, 1);
                if (pid < 0) {
                    printf("%d byte was unlocked\n\n", offset);
                }
                else {
                    printf("Cannot unlocked byte\n");
                }
                break;


            case 4: // List locked bytes
                test_bytes(file);
                break;

            case 5: // Read byte
                offset = get_byte_index(file_size);
                lseek(file, offset, SEEK_SET);
                if (lock_type(file, offset) != F_RDLCK) {
                    if (read(file, &buffer, 1) < 0) {
                        printf("Cannot read a character\n");
                    }
                    else {
                        printf("Read %c \n", buffer);
                    }
                }
                else {
                    printf("File is write only!\n");
                }
                break;

            case 6: // Write byte
                scanf("%c",&buffer);
                scanf("%c",&buffer);
                offset = get_byte_index(file_size);
                lseek(file, offset, SEEK_SET);
                if (lock_type(file, offset) != F_WRLCK) {
                    if (write(file, &buffer, 1) < 0) {
                        printf("Cannot enter a character at this place!\n");
                    }
                    else {
                        printf("Entered: %c\n", buffer);
                    }
                }
                else {
                    printf("File is read only!\n");
                }
            case 7:
                print_option();
                break;
            default:
                return;

        }
    }

}

