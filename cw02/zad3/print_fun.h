//
// Created by pkalawski on 12.03.16.
//

#ifndef SETTHEFILELOCK_PRINT_FUN_H
#define SETTHEFILELOCK_PRINT_FUN_H

#include <stdio.h>
#include <fcntl.h>
#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include "print_fun.h"


void print_option();

void check_bytes(int file, int file_size);

#endif //SETTHEFILELOCK_PRINT_FUN_H
