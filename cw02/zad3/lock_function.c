//
// Created by pkalawski on 12.03.16.
//

#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <fcntl.h>


int lock_reg(int fd, int cmd, int type, off_t offset, int whence, off_t len)
{
    struct flock struct_flock;

    struct_flock.l_type = (short) type;
    struct_flock.l_start = offset;
    struct_flock.l_whence = (short) whence;
    struct_flock.l_len = len;

    return (fcntl(fd, cmd, &struct_flock));
}



pid_t lock_test(int fd, int type, off_t offset, int whence, int len)
{
    struct flock struct_flock;

    struct_flock.l_type = (short) type;
    struct_flock.l_start = offset;
    struct_flock.l_whence = (short) whence;
    struct_flock.l_len = len;
    if(fcntl(fd, F_GETLK, &struct_flock) < 0)
    {
        int error_code = errno;
        fprintf(stderr, "Blad fcntl\n");
        exit(error_code);
    }

    if(struct_flock.l_type == F_UNLCK)
        return 0;

    return struct_flock.l_pid;
}


int lock_read(int file,int  offset,int  whence, int len){

    int tmp = lock_reg(file,F_SETLK,F_RDLCK,offset,whence,len);
    return tmp;
}

int lock_write(int file,int  offset,int  whence, int len){

    int tmp = lock_reg(file,F_SETLK,F_WRLCK,offset,whence,len);
    return tmp;
}

int unlock(int file,int  offset,int  whence, int len){

    int tmp = lock_reg(file,F_SETLK,F_WRLCK,offset,whence,len);
    return tmp;
}

pid_t lockable_write(int file,int offset,int whence,int len){
    int tmp = lock_test(file,F_RDLCK,offset,whence,len);
    return tmp;
}

pid_t lockable_read(int file,int offset,int whence,int len){
    int tmp = lock_test(file,F_RDLCK,offset,whence,len);
    return tmp;
}

short lock_type(int fd, int byte)
{
    struct flock lock;

    lock.l_type = F_RDLCK;
    lock.l_start = byte;
    lock.l_whence = SEEK_SET;
    lock.l_len = 1;

    if(fcntl(fd, F_GETLK, &lock) < 0)
    {
        int error_code = errno;
        fprintf(stderr, "Blad fcntl\n");
        exit(error_code);
    }

/*    printf("Byte: %d:  %d\n",byte,lock.l_type);*/
    return lock.l_type;
}



void test_bytes(int fd)
{
    int i = 0;

    int end = (int) lseek(fd, 0, SEEK_END);

    for(i = 0 ; i < end ; i++)
    {

        int type = lock_type(fd, i);
        if(type == F_WRLCK)
            printf("Byte %d is read only. Owner: PID=%d\n", i, lockable_write(fd, i, SEEK_SET, 1));
        if(type == F_RDLCK)
            printf("Byte %d is write only. Owner: PID=%d\n", i, lockable_read(fd, i, SEEK_SET, 1));
    }
}




