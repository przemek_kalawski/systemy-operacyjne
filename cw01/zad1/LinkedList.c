//
// Created by pkalawski on 02.03.16.
//


#include "LinkedList.h"


void add(node_t ** head, Contact_t * contact){

    node_t * tmp = malloc(sizeof(node_t));
    tmp->contact = contact;
    tmp->next = tmp->previous = NULL;

    if(!(*head))
        (*head) = tmp;
    else{
        tmp->next = *head;
        (*head)->previous = tmp;
        (*head) = tmp;
    }
}

void find_node(const node_t *head,Contact_t *contact) {
    if(contact == NULL){
        printf("Contact not exist!");
        return;
    }

    if(head == NULL){
        printf("In this list contact not exist!");
        return;
    }

    if(compare_contacts(head->contact,contact)){
        printf("%s\n", head->contact->last_name);
        return;
    }

    find_node(head->next,contact);
}

int compare_contacts(Contact_t *first, Contact_t *second) {


    if(strcmp(first->first_name,second->first_name))
        return 0;
    if (strcmp(first->last_name,second->last_name))
        return 0;
    if (strcmp(first->address,second->address))
        return 0;
    if (strcmp(first->birthday, second->birthday))
        return 0;
    if (strcmp(first->e_mail , second->e_mail))
        return 0;
    if(strcmp(first->phone, second->phone))
        return 0;
    return 1;
}


void printList(node_t *head)
{
    while (head)
    {
        printf("%s,  ",head->contact->last_name);
        head = head->next;
/*        assert(head == NULL);*/
    }
    printf("\n");
}

node_t *merge(node_t *first, node_t *second) {
    if(!first)
        return  second;

    if(!second)
        return first;
    if(*first->contact->last_name < *second->contact->last_name){
        first->next = merge(first->next,second);
        first->next->previous = first;
        first->previous = NULL;
        return first;
    }
    else{
        second->next = merge(first,second->next);
        second->next->previous = second;
        second->previous = NULL;
        return second;
    }

}

node_t *merge_sort(node_t *head) {
    if(!head || !head->next)
        return head;
    struct node *second = split(head);

    head = merge_sort(head);
    second = merge_sort(second);

    return merge(head, second);
}

node_t *split(node_t *head) {
    node_t *fast = head,*slow = head;
    while(fast->next && fast->next->next){
        fast = fast->next->next;
        slow = slow->next;
    }

    node_t *tmp = slow->next;
    slow->next = NULL;
    return tmp;
}


void remove_node1(node_t **head, node_t *del) {

    node_t * current = *head;

    if(current == NULL || del == NULL)
        return;


    if(compare_contacts(current->contact,del->contact)){
        node_t * node_t1 = current;
        current->previous = NULL;
        *head = current->next;
        free(node_t1);
        return;
    }

    while(current->next !=NULL && !compare_contacts(current->next->contact,del->contact)){
        current = current->next;
    }

    if(current->next == NULL){
        printf("\nElement is not present in the list\n");
        return;
    }

    node_t * tmp = current->next;

    if(tmp->next == NULL){
        current->next = NULL;
    }
    else{
        current->next = tmp->next;
        (current->next)->previous = tmp->previous;
    }

    tmp->previous = current;

    free(tmp);

}

void remove_t(node_t **root) {

    node_t * tmp = *root;
    if(*root != NULL){
        *root = tmp->next;
        free(tmp);
        remove_t(root);
    }


}

//wypisanie błędu przy alokacji

void error_cant_allocate()
{
    puts("cant allocate memory");
    puts(strerror(errno));
    exit(1);
}


char *test_utils_clone(char *a)
{
    char *b = malloc(strlen(a) + 1);
    if (!b) {
        error_cant_allocate();
    }

    strcpy(b, a);

    return b;
}




