//
// Created by pkalawski on 02.03.16.
//


#include "Contact.h"



Contact_t * create_contact(){
    Contact_t * contact = malloc(sizeof(struct Contact));
    contact->first_name = malloc(30 * sizeof(char));
    contact->address = malloc(30* sizeof(char));
    contact->birthday = malloc(30 * sizeof(char));
    contact->e_mail = malloc(30* sizeof(char));
    contact->last_name = malloc(30* sizeof(char));
    contact->phone = malloc(30 * sizeof(char));

    return contact;
}


Contact_t *add_first_name(Contact_t *contact, char * string) {
    if(!contact){
        printf("Bad allocation memory!");
        return contact;
    }
    if(strlen(string) > 30){
        contact->first_name = realloc(contact->first_name,strlen(string)*sizeof(char));
    }
    contact->first_name = string;
    return contact;
}

Contact_t * add_last_name(Contact_t * contact, char * string){
    if(!contact){
        printf("Bad allocation memory!");
        return contact;
    }
    if(strlen(string) > 30){
        contact->last_name = realloc(contact->last_name,strlen(string)*sizeof(char));
    }
    contact->last_name = string;
    return contact;
}

Contact_t * add_birthday(Contact_t * contact, char * string){
    if(!contact){
        printf("Bad allocation memory");
        return contact;
    }
    if(strlen(string) > 30){
        contact->birthday = realloc(contact->birthday,strlen(string)*sizeof(char));
    }
    contact->birthday = string;
    return contact;
}

Contact_t * add_e_mail(Contact_t * contact, char * string){
    if(!contact){
        printf("Bad allocation memory");
        return contact;
    }
    if(strlen(string) > 30){
        contact->e_mail = realloc(contact->e_mail,strlen(string)*sizeof(char));
    }
    contact->e_mail = string;
    return contact;
}


Contact_t *add_phone(Contact_t *contact, char *string) {
    if(!contact){
        printf("Bad allocation memory");
        return contact;
    }
    if(strlen(string) > 30){
        contact->phone = realloc(contact->phone,strlen(string)*sizeof(char));
    }
    contact->phone = string;
    return contact;
}

Contact_t *add_address(Contact_t *contact, char *string) {
    if(!contact){
        printf("Bad allocation memory");
        return contact;
    }
    if(strlen(string) > 30){
        contact->address = realloc(contact->address,strlen(string)*sizeof(char));
    }
    contact->address = string;
    return contact;
}

Contact_t *new_contact_t(char *first_name, char *last_name, char *birthday, char *e_mail, char *phone, char *address) {

    Contact_t * new_contact = malloc(sizeof(Contact_t));
    new_contact->first_name = first_name;
    new_contact->last_name = last_name;
    new_contact->birthday = birthday;
    new_contact->e_mail = e_mail;
    new_contact->phone = phone;
    new_contact->address = address;

    return new_contact;


}
