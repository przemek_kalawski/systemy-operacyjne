//
// Created by pkalawski on 02.03.16.
//

#ifndef FIRSTPROJECT_CONTACT_H
#define FIRSTPROJECT_CONTACT_H

#include <stdlib.h>
#include <string.h>
#include <stdio.h>

struct Contact{
        char * first_name;
        char * last_name;
        char * birthday;
        char * e_mail;
        char * phone;
        char * address;
};

typedef struct Contact Contact_t;

    Contact_t * create_contact();

    Contact_t * add_first_name(Contact_t *contact, char * string);

    Contact_t * add_last_name(Contact_t * contact, char * string);

    Contact_t * add_birthday(Contact_t * contact, char * string);

    Contact_t * add_e_mail(Contact_t * contact, char * string);

    Contact_t * add_phone(Contact_t * contact, char * string);

    Contact_t * add_address(Contact_t * contact, char * string);

    Contact_t * new_contact_t(char * first_name, char * last_name, char * birthday, char * e_mail, char * phone, char * address);


#endif //FIRSTPROJECT_CONTACT_H
