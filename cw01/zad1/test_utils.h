#ifndef TESTUTILS_H_
#define TESTUTILS_H_

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <sys/resource.h>
#include "error.h"


/**
 * Prints a control point
 */
void test_utils_control_point();

#endif
