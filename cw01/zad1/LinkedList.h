//
// Created by pkalawski on 02.03.16.
//


#ifndef FIRSTPROJECT_LINKEDLIST_H
#define FIRSTPROJECT_LINKEDLIST_H

#include <stddef.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include "Contact.h"

struct node{
        struct node * next;
        struct node * previous;
        Contact_t *contact;
};


typedef struct node node_t;

void add(node_t ** head, Contact_t * contact);

void find_node(const node_t * head,Contact_t * contact);

int compare_contacts(Contact_t * first, Contact_t * second);

void printList(node_t *head);

void remove_node1(node_t **head, node_t * del);

//////// MergeSort

node_t * merge(node_t * first, node_t * second);

node_t * merge_sort(node_t *head);

node_t *split(node_t * head);

void remove_t(node_t ** root);

//błąd przy alokacji pamięci

void error_cant_allocate();

char *test_utils_clone(char *a);

#endif //FIRSTPROJECT_LINKEDLIST_H
