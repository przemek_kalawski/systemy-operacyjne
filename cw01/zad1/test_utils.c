
#include "test_utils.h"

static void _test_utils_print_usage(double real, double user, double sys)
{
    printf(
            "| real\t%fs\n"
                    "| user\t%fs\n"
                    "| sys\t%fs\n",
            real, user, sys
    );
}

void test_utils_control_point()
{
    static int counter = 0;
    static double	real, user, sys,
            real_first, user_first, sys_first,
            real_before, user_before, sys_before;
    struct rusage rusage;
    clock_t my_clock;

    counter += 1;

    // Update before control point
    if (counter > 1) {
        real_before = real;
        user_before = user;
        sys_before = sys;
    }

    // Get current control point values
    getrusage(RUSAGE_SELF, &rusage);
    my_clock = clock();

    real = my_clock / (double)CLOCKS_PER_SEC;
    user = rusage.ru_utime.tv_sec + rusage.ru_utime.tv_usec / (double)10e6;
    sys = rusage.ru_stime.tv_sec + rusage.ru_stime.tv_usec / (double)10e6;

    // Save first control point
    if (counter == 1) {
        real_first = real;
        user_first = user;
        sys_first = sys;
    }

    printf("\n* Control point %d\n", counter);
    _test_utils_print_usage(real, user, sys);

    if (counter > 1) {
        printf("* Difference beetwen 1 and %d\n", counter);
        _test_utils_print_usage(real - real_first, user - user_first, sys - sys_first);
    }

    if (counter > 2) {
        printf("* Difference beetwen %d and %d\n", counter - 1, counter);
        _test_utils_print_usage(real - real_before, user - user_before, sys - sys_before);
    }

    printf("\n");
}
