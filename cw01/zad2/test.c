//
// Created by pkalawski on 07.03.16.
//


#include "test_utils.h"
#include "LinkedList.h"


int main(){

    test_utils_control_point();

     Contact_t * contact2 = new_contact_t("Piotr","Okalk","1999-12-23","qerty@bbb.com", "908-039-098","Mickiewicza 21");

    char * first_name [] = {
            "Marek", "James", "Roberto", "Emre", "Simon", "Daniel", "Phillipe"
    };

    char * last_name [] = {
            "Can", "Firmino", "Skrtel", "Sakho", "Milner", "Minolet", "Clyne"
    };
    char * e_mails [] = {
            "qwert@wer.pl","kjwf@jbjf.ok","ughusif@wp.pl","piuy@gmail.com", "erwet@qw.pl",
            "flanagan@gmail.com","milner@live.com"
    };

    char *dates_of_birth[] = {
            "1999-12-23", "1982-01-24", "2021-04-03", "2000-01-01",
            "1900-09-23", "1987-09-22", "1999-01-01"
    };

    char *addresses[] = {
            "Mickiewicza 21", "Slowackiego 11", "Kawiory 21", "Rusznikarska 14", "Glowackiego 24",
            "Piaski Nowe 69", "Karmelicka 1", "Halszki 33"
    };
    char *phones[] = {
            "730-000-000", "111-111-111", "222-232-235", "987-345-345",
            "000-000-000", "198-234-123", "123-321-465"
    };

    int samples = 7;

    printf("Creating list head\n");
    node_t * head = NULL;

    printf("Generating and inserting contacts into list\n");

    for (int i = 0; i < 10000; ++i) {
        Contact_t *contact_t = new_contact_t(
                test_utils_clone(first_name[rand()%samples]),
                test_utils_clone(last_name[rand()%samples]),
                test_utils_clone(dates_of_birth[rand()%samples]),
                test_utils_clone(e_mails[rand()%samples]),
                test_utils_clone(phones[rand()%samples]),
                test_utils_clone(addresses[rand()%samples])
        );

        add(&head,contact_t);
    }

    printf("Testing after add all contacts into list.\n");

    test_utils_control_point();

/*    (*printList)(head);*/

    find_node(head,contact2);

/*    (*printList)(head);*/

    remove_node1(&head,head->next);

    printf("Testing after find and remove node from list.\n");

    test_utils_control_point();

    head = merge_sort(head);

    printf("Testing after sort the list.\n");

/*    printList(head);*/

    test_utils_control_point();

    return 0;
}