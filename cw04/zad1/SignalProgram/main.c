#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <stdbool.h>


int liczba_powtorzen;
char * text = NULL;
char * wspak = NULL;
bool  kierunek = true; // Jezeli "true" wypisuj wspak, "false" - normalnie.
bool zwieksz = true;

//Liczba powtorzonych napisow
int powtorzenia_napisow = 1;

void sigtstp_handler(int s){

    if(kierunek){
        for (int i = 0; i < powtorzenia_napisow; ++i) {
            printf("%s",wspak);
        }
        printf("\n");
        kierunek = false;
    }
    else{
        for (int i = 0; i < powtorzenia_napisow; ++i) {
            printf("%s",text);
        }
        printf("\n");
        kierunek = true;
    }

    if(kierunek && zwieksz){
        powtorzenia_napisow++;
    }
    else if(!zwieksz && kierunek){
        powtorzenia_napisow--;
    }

    if(powtorzenia_napisow == 1){
        zwieksz = true;
    }
    else if(powtorzenia_napisow == liczba_powtorzen){
        zwieksz = false;
    }


}

void sigint_handler(int s){
    if(s == SIGINT){
        printf("Zostal przekazany sygnał SIGINT\n");
    }
    exit(1);
}

int main(int argc, char **argv) {

    if(argc < 3){
        printf("Zbyt mało argumentów!\n");
        return -1;
    }
    int j = 0;

    liczba_powtorzen = (int) strtol(argv[1], NULL, 10);
    text = argv[2];
    wspak = malloc(sizeof(char) * strlen(text));

    for (int i = (int) (strlen(text) - 1); i >= 0 ; --i) {
        wspak[j++] = text[i];
    }


    signal(SIGINT,sigint_handler);

    struct sigaction sig;
    sig.sa_handler = sigtstp_handler;
    sigaction(SIGTSTP,&sig,NULL);

    while(1){
        pause();
    }
}