#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>

static int licznik;
static int end;

void sig_usr1_handler(int s, siginfo_t * inf, void * ptr){
    printf("Otrzymalem sygnal numer %d\n",licznik);
    licznik++;
    kill(inf->si_pid, SIGUSR1);
}


void sig_usr2_handler(int s){
    printf("Koniec otrzymywania sygnalow od Sendera!\n");
    end = 1;
}


int main() {

    licznik = 0;
    end = 0;

    struct sigaction action;
    action.sa_flags = SA_SIGINFO;
    sigfillset( &action.sa_mask);
    action.sa_sigaction = sig_usr1_handler;

    if(sigaction(SIGUSR1,&action,NULL) < 0){
        printf("Nie moge zlapac SIGUSR1\n");
    }

    if(signal(SIGUSR2, sig_usr2_handler) == SIG_ERR){
        printf("Nie moge zlapac SIGUSR2\n");
    }
    int error_code = errno;

    FILE * fp = popen("pidof Sender","r");

    if(fp == NULL){
        printf("blad w otwarciu procesu\n");
        exit(error_code);
    }
    int PID;
    fscanf(fp,"%d",&PID);
    fclose(fp);


    if(kill(PID,SIGUSR1) < 0) {
        printf("Blad w wyslaniu sygnalu\n");
        error_code = errno;
        exit(error_code);
    }

    while(!end){
        pause();
    }


    return 0;
}