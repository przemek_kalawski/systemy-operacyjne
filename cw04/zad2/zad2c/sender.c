#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>

static int oczekuj;
static int licznik;

void sigusr1_handler(int s){

    oczekuj = 0;
    licznik++;
}

void sig_usr2_handler(int s){
    printf("Koniec otrzymywania sygnalow od Catchera!\n");
    exit(1);
}



int main(int argc, char ** argv) {

    oczekuj = 1;
    licznik = 0;

    if(argc < 2){
        printf("Uzycie: %s [N]\n",argv[0]);
        exit(-1);
    }

    int counter = atoi(argv[1]);

    signal(SIGUSR1,sigusr1_handler);

    pid_t PID = fork();
    int error_code = errno;

    if(PID<0){
        fprintf(stderr, "fork error : %s\n", strerror(error_code));
        exit(error_code);
    }
    else if(PID == 0){
        if(execl("Catcher","Catcher",NULL) < 0){
            printf("blad przy tworzeniu nowego procesu potomnego!\n");
        }
        _exit(2);
    }
    else if (PID > 0){
        printf("Czekam na sygnal.\n");

        while(oczekuj){
            pause();
        }

        for (int i = 0; i < counter; ++i) {
            printf("Wysylam sygnal numer %d\n", i);
            kill(PID,SIGRTMIN + 2);

            while(oczekuj){
                pause();
            }
            oczekuj = 1;
        }

        printf("Zakonczenie wysylania sygnalow\n");
        kill(PID, SIGRTMIN+ 1);

    }

    signal(SIGUSR2,sig_usr2_handler);

}