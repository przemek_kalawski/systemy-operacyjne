#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>

static int licznik;
static int end;

void sig_usr1_handler(int s){
    printf("Otrzymalem sygnal numer %d\n",licznik);
    licznik++;
}

void sigint_handler(int s){
    printf("Zakoncz program");
    exit(1);
}

void sig_usr2_handler(int s){
    printf("Koniec otrzymywania sygnalow od Sendera!\n");
    end = 1;
}


int main() {

    licznik = 0;
    end = 0;

    if(signal(SIGUSR1, sig_usr1_handler) == SIG_ERR){
        printf("Nie moge zlapac SIGUSR1\n");
    }

    if(signal(SIGUSR2, sig_usr2_handler) == SIG_ERR){
        printf("Nie moge zlapac SIGUSR2\n");
    }
    int error_code = errno;

    FILE * fp = popen("pidof Sender","r");

    if(fp == NULL){
        printf("blad w otwarciu procesu\n");
        exit(error_code);
    }
    int PID;
    fscanf(fp,"%d",&PID);
    fclose(fp);


    if(kill(PID,SIGUSR1) < 0) {
        printf("Blad w wyslaniu sygnalu\n");
        error_code = errno;
        exit(error_code);
    }

    while(!end){
        pause();
    }


    return 0;
}